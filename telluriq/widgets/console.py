from kivy.uix import boxlayout, scrollview
import logging.handlers
import socketserver
import threading
import json
import itertools


class LogsTCPHandler(socketserver.BaseRequestHandler):
    """Request handler for logs

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.

    Extends:
        socketserver.BaseRequestHandler
    """

    logger = logging.getLogger("console")

    def handle(self):
        # self.request is the TCP socket connected to the client
        sock = self.request

        # How to parse a JSON object from a socket that receives bytes
        # +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
        # |  Packet Size  |    JSON Packet    |  Packet Size  |    etc..
        # +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+

        # number of bytes that encodes the JSON packet size
        # as a little-endian unsigned integer
        n_bytes = 4

        while data := sock.recv(n_bytes):
            try:
                packet_size = int.from_bytes(data, "little")
                data = sock.recv(packet_size)
                obj = json.loads(data)
            except ValueError as e:
                self.logger.error(f"telluriq: logs: ValueError: {str(e)}")
                continue
            except json.decoder.JSONDecodeError as e:
                self.logger.error(f"telluriq: logs: JSONDecodeError: {str(e)}")
                continue
            except Exception as e:
                self.logger.error(f"telluriq: logs: error: {str(e)}")
                continue

            # check if json object has required keys
            for k in ("level", "message", "server"):
                if k not in obj:
                    self.logger.error(
                        f"telluriq: logs: record from `{obj['server']}` has no `{k}` key"
                    )
                    continue

            # call logger method for received object
            funk = getattr(self.logger, obj["level"], None)
            if funk:
                funk(obj["message"])
            else:
                self.logger.error(
                    f"telluriq: logs: logger has no method `{obj['level']}`"
                )
                continue


class TCPServer(socketserver.TCPServer):
    allow_reuse_address = True


class LogsTCPServerThread(threading.Thread):

    logger = logging.getLogger("console")

    def __init__(self, host="", port=9001, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.host, self.port = host, port
        self.server = TCPServer((self.host, self.port), LogsTCPHandler)

    def run(self):
        self.server.serve_forever()

    def stop(self):
        self.server.server_close()


class Console(boxlayout.BoxLayout):
    server_thread = LogsTCPServerThread()
    logger = logging.getLogger("console")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.server_thread.start()

    def filter(self, log_level=None):
        """Filter log records

        Filter log records matching given level

        Keyword Arguments:
            log_level {str} -- The log level to filter records for (default: {None})
        """
        label = self.ids.drawer.children[0]
        handler = self.logger.handlers[0]
        buffer = handler.buffer
        if log_level is None:
            label.text = "\n".join([handler.format(record) for record in buffer])
        else:
            label.text = "\n".join(
                [
                    handler.format(record)
                    for record in buffer
                    if log_level in (record.levelno, record.levelname)
                ]
            )


class LabelHandler(logging.handlers.BufferingHandler):

    colors = {
        "DEBUG": "ffffff",
        "INFO": "ffffff",
        "WARNING": "00ffff",
        "ERROR": "f87a7a",
        "CRITICAL": "f87af2",
    }

    prefixes = {
        "DEBUG": ">>",
        "INFO": ">>",
        "WARNING": "!>",
        "ERROR": "x>",
        "CRITICAL": ")>",
    }

    def __init__(self, root, capacity=512):
        super().__init__(capacity)
        self.root = root

    def flush(self):
        if len(self.buffer):
            self.buffer.pop(0)

    def shouldFlush(self, record):
        return len(self.buffer) >= self.capacity

    def format(self, record):
        """formats a record according to handler internal style

        This adds `colors` and `prefixes` to each record

        Arguments:
            record {dict} -- The log record to format
        """
        return (
            f"[color={self.colors.get(record.levelname, 'ffffff')}]"
            f"{self.prefixes.get(record.levelname, '>>')} "
            f"{record.getMessage()}"
            "[/color]"
        )

    def emit(self, record):
        label = self.root.children[0].children[0]
        scroll_view = self.root.children[0]
        # compute available view size on the y axis (minus padding)
        available = scroll_view.size[1] - 2 * scroll_view.padding
        # auto scroll as buffer gets filled
        scroll_view.scroll_y = 0 if label.size[1] > available else 1
        # format record
        formatted = self.format(record)

        if self.shouldFlush(record):
            self.flush()

        # append formatted record to buffer
        super().emit(formatted)
        # write buffer to label text
        label.text = "\n".join(log for log in self.buffer)


class ConsoleView(boxlayout.BoxLayout):

    logger = logging.getLogger("console")

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        handler = LabelHandler(self)
        self.logger.addHandler(handler)
        # enforce info logging level
        self.logger.setLevel(logging.DEBUG)
