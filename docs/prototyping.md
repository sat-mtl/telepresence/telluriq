# Prototyping
This is old documentation for [tkinter](https://docs.python.org/3/library/tkinter.html) but we concider useful to keep for futur works that requires prototypes.

## Extra requirements
| Name                                                | Version | Description                                    |
|-----------------------------------------------------|---------|------------------------------------------------|
| [Tk](https://tkdocs.com/tutorial/install.html)      | ≥ 8.6.8 | Required GUI toolkit                           |

## Compile a [Figma](https://www.figma.com/) document to [Tkinter](https://docs.python.org/3/library/tkinter.html) widgets
`Telluriq` was designed with [Figma](https://www.figma.com/) following the [Tkinter-Designer](https://github.com/ParthJadhav/Tkinter-Designer/blob/master/docs/instructions.md) naming specification.

In order to compile a [Figma](https://www.figma.com/) document to [Tkinter](https://docs.python.org/3/library/tkinter.html) widgets, use the `tkdesigner` package:

```bash
pip install tkdesigner
```

We are now able to compile [Tkinter](https://docs.python.org/3/library/tkinter.html) widgets to a `build` folder:

```bash
tkdesigner <project_url> <figma_token>
```

You can run the resulting [Tkinter](https://docs.python.org/3/library/tkinter.html) application as follows:

```bash
python3 build/gui.py
```
