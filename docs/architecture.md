<script crossorigin src="https://unpkg.com/mermaid/dist/mermaid.min.js"></script>
<script>
  mermaid.initialize({ startOnLoad: true, theme: 'dark' });
  mermaid.flowchartConfig = { width: '100%' };
  mermaid.sequenceConfig = { mirrorActors: true };
</script>


# Architecture

Telluriq is designed to interract with the haptic floor through an OSC API. We provide a flow diagram in order to have a high view of the possibilities given by Telluriq and the haptic floor.

![Telluriq flow](assets/svg/flow.svg)

A sequence diagram summarize the communication betwwen Telluriq and the haptic floor:

<div class="mermaid">
sequenceDiagram
  User->>Telluriq: Calibrate an actuator
  Telluriq->>DBoxer: Send OSC messages with random positions
  Note right of Telluriq: Telluriq rocks!
  Telluriq-->>User: Change the Connexion button status
  loop OSC flooding
    DBoxer->>Telluriq: Send all logs with status changes
  end
</div>
