.PHONY: install setup-dev init build lint check-format format start watch clean

# Install the required python dependencies
install:
	flit install

# Install the dev dependencies
setup-dev:
	python3 -m pip install -r requirements-dev.txt

# Install the doc dependencies
setup-doc:
	python3 -m pip install -r requirements-doc.txt

# Initialize the virtualenv shell
init:
	if [ ! -d ./env ]; then python3 -m virtualenv env; fi
	rm activate && ln -s ./env/bin/activate

# Build the application
build:
	flit build

# Lint the code base
lint:
	python3 -m flake8 telluriq

# Check the code format (without reformatting)
check-format:
	python3 -m black --check telluriq

# Format the code base
format:
	python3 -m black telluriq

# Start the application
start:
	python3 -m telluriq

# Watch the code changes and rerun the application
watch:
	ls telluriq/*.py | entr -r make start

# Generate the project documentation
doc:
	mkdocs build --clean

# Serve the documentation on localhost
serve-doc:
	mkdocs serve
